	chip 65816
PDD4	equ	$DF24
PD7	equ	$DF23
SSCR1	equ	$DF41


	longa off
	longi off

PD72	equ	$04

	org $1000
	sei
	cld
	ldx #$FF
	txs

	sec
	xce

?loop	lda #PD72
	tsb PD7

	jsr Delay

	lda #PD72
	trb PD7

	jsr Delay

	bra ?loop


Delay
        phx
        phy
        ldy #$00
?L2     ldx #$00
?L1     inx
        bne ?L1
        iny
        bne ?L2
        ply
        plx
        rts
