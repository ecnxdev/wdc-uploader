PDD5	equ	$001F
PD5	equ	$001D

PD57	equ	$80

	org $1000

	lda #PD57
	tsb PDD5

?loop	lda #PD57
	tsb PD5

	jsr LDelay

	lda #PD57
	trb PD5

	jsr LDelay

	bra ?loop



LDelay
	ldx #$05
?L1	jsr Delay
	dex
	bne ?L1
	rts

Delay
	phx
	phy
	ldy #$00
?L2	ldx #$00
?L1	inx
	bne ?L1
	iny
	bne ?L2
	ply
	plx
	rts
