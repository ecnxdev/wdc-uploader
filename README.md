# README #

This README Contains all information necessary for configuring and using the WDC uploader.py python script

### W65C02, W65C816, W65C134 and W65C265 boards Uploader script ###

* This uploader works on linux, mac and windows platform
* Version 1.0.0

## Setup, Usage and Configuration ##

You are required to install these important modules. 
```
pyserial, binascii
```
Run the command below to view help information
```
  python uploader.py -h
```
Basic usage: 
```
uploader.py [-h] [-b BAUDRATE] [-d DEVICE] [-x EXECUTE] [-v] [-t {02,134,816,265}] FILENAME
```

Upload file to WDC board.

### positional arguments: ###
  FILENAME              file name to upload to board

### optional arguments: ###
*  -h, --help                       show this help message and exit
*  -b BAUDRATE, --baudrate BAUDRATE set baud rate, default: 9600
*  -d DEVICE, --device DEVICE       set the serial port device
*  -x EXECUTE, --execute EXECUTE    set the address where the code will execute from
*  -v, --verbose                    Switch on verbose to get debug messages
*  -t {02,134,816,265}, --type {02,134,816,265} set board type, one of {02, 134, 816, 265}, Will try
                        to auto detect if not provided

NOTE: This uploads compiled code to all the W65CO2, W65C134, W65C265 and
W65816, 8 and 16 bit processors

### License ###

* MIT License
