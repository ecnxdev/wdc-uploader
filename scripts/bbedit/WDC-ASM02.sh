#!/bin/bash
WDCTOOLS="/Users/jihad/WDCTools/bin"
WDCPATH="${BB_DOC_PATH%/*}"  	#path
FILE="${BB_DOC_NAME%.*}"  	#filename without extension
EXT="${BB_DOC_NAME##*.}" 	#extension
PORT="/dev/tty.usbserial-A902X06W"
EXEC="2000"

cd $WDCPATH

if wine $WDCTOOLS/WDC02AS.exe -l $FILE.asm
then
	if wine $WDCTOOLS/WDCLN.exe -HZ $FILE
	then
		if $WDCTOOLS/uploader.py -d $PORT $FILE.bin -x $EXEC
		then
		/Applications/Zterm/ZTerm.app/Contents/MacOS/ZTerm\ Release
		echo
		fi
	fi
fi
